/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;
import java.io.*;

/**
 *
 * @author user
 */
public class JavaApplication5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String path = "1.txt";
	BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	String str;
        FileWriter write = new FileWriter(path, true);
	while ((str = input.readLine()) != null)
        {
            if (str.isEmpty())
            {
                break;
            }
            write.write(str);
        }
        write.close();
    }
    
    
}
