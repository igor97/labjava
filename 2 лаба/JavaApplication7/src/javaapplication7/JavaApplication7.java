/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication7;
import java.io.*;
import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author user
 */
public class JavaApplication7 {

    /**
     * @param args the command line arguments
     */
    
    static public void  read() throws FileNotFoundException, IOException{
        RandomAccessFile file = new RandomAccessFile("1.txt", "r");
        int size=0;
        int str;
        size=(int) file.length();
        if(size==0)
        {
            while ((str = file.read()) != -1)
            {
                System.out.print((char)str);
            }
        }
        else
        {
           final Random random = new Random();
           file.seek(random.nextInt(size));
             while ((str = file.read()) != -1)
            {
                System.out.print((char)str);
            }
        }
        file.close();
    }
    
    static public void wreat() throws FileNotFoundException, IOException{
        RandomAccessFile file = new RandomAccessFile("1.txt", "rw");
        int size=0;
        String str;
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        size=(int) file.length();
        if(size==0)
        {
                while ((str = input.readLine())!= null)
                {
                    if (str.isEmpty())
                    {
                        break;
                    }
                    file.writeChars(str);
                }
                
        }
        else
        {
            final Random random = new Random();
            file.seek(random.nextInt(size));
            while ((str = input.readLine()) != null)
                {
                    if (str.isEmpty())
                    {
                        break;
                    }
                file.writeChars(str);
                }
           
        }
        file.close();
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Выбирите действие: ");
        System.out.println("1 - Запись в файл ");
        System.out.println("2 - Чтение из файла ");// TODO code application logic here
        int buf;
        buf=in.nextInt();
        if(buf==2){
            read();
        }
        if(buf==1){
            wreat();
        }
    }
    
  
    
}
