/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication81;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class JavaApplication81 {
public static ArrayList<Laptop> laptops;
    public static Scanner console;


    public static void add() throws IOException{
        Laptop tmp = new Laptop();
        System.out.println("Enter manufacturer name: ");
        tmp.setManufacturerName(console.nextLine());

        System.out.println("Enter model name: ");
        tmp.setModelName(console.nextLine());

        System.out.println("Enter CPU name: ");
        tmp.setCPUName(console.nextLine());

        System.out.println("Enter CPU power(GHz): ");
        tmp.setCpuPower(console.nextDouble());

        System.out.println("Enter RAM capacity(MB): ");
        tmp.setRAMCapacity(console.nextInt());
        console.nextLine();

        System.out.println("Enter video card name: ");
        tmp.setVideoModel(console.nextLine());

        System.out.println("Enter video card memory(MB): ");
        tmp.setVideoCapacity(console.nextInt());
        console.nextLine();

        laptops.add(tmp);
    }

    public static void remove() throws IOException {
        print();
        int i;
        while(true) {
            System.out.println("Enter position of laptop to be deleted(0 to exit): ");
            i = console.nextInt();
            console.nextLine();
            if((i >= 0) && (i <= laptops.size()))
                break;
            else
                System.out.println("Invalid position");
        }
        if(i == 0)
            return;
        laptops.remove(i - 1);
    }

    public static void edit() throws IOException {
        print();
        int i;
        while(true) {
            System.out.println("Enter position of laptop to be edited(0 to exit): ");
            i = console.nextInt();
            if((i >= 0) && (i <= laptops.size())) {
                break;
            } else
                System.out.println("Invalid position");
        }
        if(i == 0)
            return;
        int ch;
        do {
            console.nextLine();
            System.out.println(laptops.get(i - 1));
            System.out.println("Choose operation: ");
            System.out.println("1 - Edit manufacturer name.");
            System.out.println("2 - Edit model name.");
            System.out.println("3 - Edit CPU name.");
            System.out.println("4 - Edit CPU power.");
            System.out.println("5 - Edit RAM capacity.");
            System.out.println("6 - Edit video card name.");
            System.out.println("7 - Edit video card memory.");
            System.out.println("0 - Exit");
            ch = console.nextInt();
            console.nextLine();
            switch (ch) {
                case 1:
                    laptops.get(i - 1).setManufacturerName(console.nextLine());
                    break;
                case 2:
                    laptops.get(i - 1).setModelName(console.nextLine());
                    break;
                case 3:
                    laptops.get(i - 1).setCPUName(console.nextLine());
                    break;
                case 4:
                    laptops.get(i - 1).setCpuPower(console.nextDouble());
                    break;
                case 5:
                    laptops.get(i - 1).setRAMCapacity(console.nextInt());
                    break;
                case 6:
                    laptops.get(i - 1).setVideoModel(console.next());
                    break;
                case 7:
                    laptops.get(i - 1).setVideoCapacity(console.nextInt());
                    break;
            }
        }while(ch != 0);
    }

    public static void print() throws IOException {
        int i = 1;
        for(Laptop l : laptops) {
            System.out.println("" + i + ") " + l);
            i++;
        }
    }


    public static void main(String[] args) {
        console = new Scanner(System.in).useLocale(Locale.US);
        String fileName;
        try {
            String s;
            boolean justCreated = false;
            do {
                s = "y";
                System.out.println("Enter data base file name:");
                fileName = console.nextLine();
                File f = new File(fileName);
                if (!f.exists() || f.isDirectory()) {
                    System.out.println("Data base file not found. Create new file?(y/n)");
                    s = console.nextLine();
                    if (s.equals("y")) {
                        f.createNewFile();
                        justCreated = true;
                    }
                }
            } while (s.equals("n"));

            laptops = new ArrayList<>();
            if(!justCreated) {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
                laptops = (ArrayList<Laptop>)ois.readObject();
                ois.close();
            }

            do {
                System.out.println("----------------------------------");
                System.out.println("Choose operation");
                System.out.println("1 - add");
                System.out.println("2 - remove");
                System.out.println("3 - edit");
                System.out.println("4 - print");
                System.out.println("0 - exit");
                s = console.nextLine();
                switch (s) {
                    case "1":
                        add();
                        break;
                    case "2":
                        remove();
                        break;
                    case "3":
                        edit();
                        break;
                    case "4":
                        print();
                        break;
                    case "0":
                        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
                        oos.writeObject(laptops);
                        oos.close();
                        break;
                }
                System.out.println("----------------------------------");
            }while(!s.equals("0"));
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    
}
