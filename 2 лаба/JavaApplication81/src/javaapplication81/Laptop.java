/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication81;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Laptop implements Serializable{
    String modelName, manufacturerName, CPUName, videoModel;
    double cpuPower;
    int RAMCapacity, videoCapacity;

    @Override
    public String toString() {
        return manufacturerName + " " + modelName + ", " + CPUName + " " + cpuPower + " GHz, " + RAMCapacity + " MB RAM, " + videoModel + " " + videoCapacity + " MB";
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getCPUName() {
        return CPUName;
    }

    public void setCPUName(String CPUName) {
        this.CPUName = CPUName;
    }

    public String getVideoModel() {
        return videoModel;
    }

    public void setVideoModel(String videoModel) {
        this.videoModel = videoModel;
    }

    public double getCpuPower() {
        return cpuPower;
    }

    public void setCpuPower(double cpuPower) {
        this.cpuPower = cpuPower;
    }

    public int getRAMCapacity() {
        return RAMCapacity;
    }

    public void setRAMCapacity(int RAMCapacity) {
        this.RAMCapacity = RAMCapacity;
    }

    public int getVideoCapacity() {
        return videoCapacity;
    }

    public void setVideoCapacity(int videoCapacity) {
        this.videoCapacity = videoCapacity;
    }
}
