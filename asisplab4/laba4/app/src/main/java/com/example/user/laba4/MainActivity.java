package com.example.user.laba4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.SearchView;
import android.widget.Toast;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.content.Intent;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


public class MainActivity extends ActionBarActivity {
    private final static String FILENAME = "sample.txt"; // имя файла
    private EditText mEditText;
    private EditText textPole;

    // private TextView mtextView;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.stsat);
        setContentView(R.layout.activity_main);


        //setContentView(R.layout.stsat);

        // mtextView=(TextView) findViewById(R.id.textView) ;

        mEditText = (EditText) findViewById(R.id.editText);

        textPole = (EditText) findViewById(R.id.serch_edit);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                saveFile("FILENAME");
                return true;
            case R.id.app_bar_search:

                serch();
                return true;
            case R.id.stat:

                // setContentView(R.layout.stsat);
                cheac();
                return true;

            default:
                return true;
        }
    }


    // Метод для сохранения файла
    private void saveFile(String fileName) {
        try {
            OutputStream outputStream = openFileOutput(fileName, 0);
            OutputStreamWriter osw = new OutputStreamWriter(outputStream);
            osw.write(mEditText.getText().toString());
            osw.close();
        } catch (Throwable t) {
            Toast.makeText(getApplicationContext(),
                    "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void serch() {
        String buf;
        String temp;
        buf = textPole.getText().toString();
        temp = mEditText.getText().toString();
        int t = temp.indexOf(buf);
        int tmp = buf.length();
        //textArea.select(t,buf.length()+1);
        mEditText.setSelection(t, t + tmp);

        mEditText.requestFocus();


    }

    public void cheac() {
        try {
            //mtextView.setText("123");
            System.out.println("1");
            int i_k_probelov = 0;
            int i_k_slov = 0;
            int i_k_slimvolov = 0;
            int i_k_cifr = 0;
            int i_k_znacov = 0;
            int i_k_glasn = 0;
            int i_k_soglasn = 0;
            String buf;
            char temp;
            char temp2 = '1';
            System.out.println("2");
            buf = mEditText.getText().toString();
            if(buf.length()!=0) {


                System.out.println("2");
                //     System.out.println(buf);
                for (int i = 0; i < buf.length(); i++) {
                    temp = buf.charAt(i);
                    if (temp == ' ') {
                        i_k_probelov++;
                    }
                    if (Character.isLetter(temp) && (temp2 == ' ' || temp2 == '\n')) {
                        i_k_slov++;
                    }
                    if (temp == '\n' || temp == '\t') {
                        i_k_slimvolov++;
                    }
                    if (Character.isDigit(temp)) {
                        i_k_cifr++;
                    }
                    if (temp == ',' || temp == '.' || temp == '?' || temp == '!' || temp == ';' || temp == ':' || temp == '-') {
                        i_k_znacov++;
                    }
                    if (Character.isLetter(temp)) {
                        if (temp == 'A' || temp == 'a' || temp == 'E' || temp == 'e' || temp == 'I' || temp == 'i' || temp == 'O' || temp == 'o' || temp == 'U' || temp == 'u') {
                            i_k_glasn++;
                        } else {
                            i_k_soglasn++;
                        }
                    }
                    temp2 = temp;
                }

                if (Character.isLetter(buf.charAt(0))) {
                    i_k_slov++;
                }
            }


            String aString = "";
            aString += "Количество слов=";
            aString += Integer.toString(i_k_slov);
            aString += "\n";
            aString += "Количество пробелов=";
            aString += Integer.toString(i_k_probelov);
            aString += "\n";
            aString += "Количество символов=";
            aString += Integer.toString(i_k_slimvolov);
            aString += "\n";
            aString += "Количество цифр=";
            aString += Integer.toString(i_k_cifr);
            aString += "\n";
            aString += "Количество знаков=";
            aString += Integer.toString(i_k_znacov);
            aString += "\n";
            aString += "Количество гласных=";
            aString += Integer.toString(i_k_glasn);
            aString += "\n";
            aString += "Количество согласных=";
            aString += Integer.toString(i_k_soglasn);
            // aString+="\n";
            System.out.println(aString);
            //System.out.println("5");
            // mtextView.setText(aString);

            Intent intent = new Intent(MainActivity.this, AbautActiviti.class);
            intent.putExtra("string",aString);
            startActivity(intent);


        } catch (NumberFormatException e) {

        }
    }
}



/*  <activity
        android:name=".AbautActiviti"
        android:label="@string/title_activity_second_activiti"
        android:parentActivityName=".AbautActiviti"
        android:theme="@style/AppTheme.NoActionBar">
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.example.user.laba4.AbautActiviti" />
    </activity>*/