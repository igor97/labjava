/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;
import java.util.Scanner;
/**
 *Класс решает задачу данную в задании 20
 * @author Перелыгин Игорь
 */
public class JavaApplication1 {

    /**
     * @param args аргумент командной строки
     */
    
    static public class Coord
    {
        private int x=0;
        private int y=0;
        public int o_X()
        {
            return x;
        }
        public int o_Y()
        {
            return y;
        }
        public void i_X()
        {
            String buf;
            Scanner in = new Scanner(System.in);
            while(true)
            {
                System.out.println("введите x ");
                buf = in.next();
                if(isNumber(buf))
                {
                     x = Integer.parseInt(buf);
                     break;
                }
            }
            return;
        }
        public void i_Y()
        {
           String buf;
            Scanner in = new Scanner(System.in);
           
           while(true)
            {
                System.out.println("введите y ");
                buf = in.next();
                if(isNumber(buf))
                {
                     y = Integer.parseInt(buf);
                     break;
                }
            }
            return;
        }
        private boolean isNumber(String str) {
            if (str == null || str.isEmpty()) return false;
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isDigit(str.charAt(i))) return false;
            }
            
            return true;
        }
    }
    
    
    public static void main(String[] args) {
        /** Определение координаты
         * @param x- координата x
         * @param y- координата y
         */
        
        int x=0;
        int y=0;
        int temp=0;
         Scanner in = new Scanner(System.in);
         
        while(true)
        {
          
             Coord coord=new Coord();
            coord.i_X();
            coord.i_Y();
            if(coord.o_X()!=0&&coord.o_Y()!=0)
            {
                System.out.println("0");
            }
            if(coord.o_X()==0&&coord.o_Y()==0)
            {
                System.out.println("1");
            }
            if(coord.o_X()==0&&coord.o_Y()!=0)
            {
                System.out.println("2");
            }
            if(coord.o_X()!=0&&coord.o_Y()==0)
            {
                System.out.println("3");
            }
            System.out.println("введите 1 для повтора");
            temp = in.nextInt();
            if(temp!=1)break;
        }
    }
    
}
/*Даны целочисленные координаты точки на плоскости. Если точка не лежит на координатных осях, то вывести 0. 
Если точка совпадает с началом координат, то вывести 1. Если точка не совпадает с началом координат, но лежит
на оси OX или OY, то вывести соответственно 2 или 3*/