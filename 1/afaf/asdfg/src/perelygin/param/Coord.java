﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perelygin.param;
import java.util.Scanner;

/**
 *
 * @author Перелыгин Игорь
 */
public class Coord implements CoordInterface
    {
        private int x=0;
        private int y=0;
        public int o_X()
        {
            return x;
        }
        public int o_Y()
        {
            return y;
        }
        public void i_X()
        {
            String buf;
            Scanner in = new Scanner(System.in);
            while(true)
            {
                System.out.println("введите x ");
                buf = in.next();
                if(isNumber(buf))
                {
                     x = Integer.parseInt(buf);
                     break;
                }
            }
            return;
        }
        public void i_Y()
        {
           String buf;
            Scanner in = new Scanner(System.in);
           
           while(true)
            {
                System.out.println("введите y ");
                buf = in.next();
                if(isNumber(buf))
                {
                     y = Integer.parseInt(buf);
                     break;
                }
            }
            return;
        }
        public boolean isNumber(String str) {
            if (str == null || str.isEmpty()) return false;
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isDigit(str.charAt(i))) return false;
            }
            return true;
        }
    }

